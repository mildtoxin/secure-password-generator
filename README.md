# Secure Password Generator
This program was designed to produce secure passwords for any type of user. 

The password produced is dependent on how many characters the user desires for their password contains only alpha-numeric characters (e.g. A, b, 1). Passwords are typically cracked in two ways: Dictionary and Brute-Force. In Dictionary attacks, the attacker may use certain combinations
of words and phrases from some source and see if these combinations match the password granting access to the attacker. On the contrary, brute-force attacks use any combination of characters until
the password is breached. In this program, the password is created in a random assortment of characters protecting it from dictionary attacks and if the password meets a certain length, it will
also protect the user from brute-force attacks.

One modification to make the password even more secure is to add more characters than just the standard alpha-numeric character set.