#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    //Character Bank used to fill password
    char bank[62] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                    'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                    'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
                    'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7',
                    '8', '9'};

    int passwdLength; //stores user input on password length
    
    //prompt user
    cout << "Welcome to mildtoxin's Secure-Password Generator!" << endl;
    cout << "How many characters do you want your password to have?" << endl;
    
    //take in length input from user
    cin >> passwdLength;

    char* password = new char[passwdLength];

    int randomIndex; //random variable
    srand(time(0)); //ensures that random passwords are not the same
    
    //Fills Password
    for(int i = 0; i < passwdLength; i++)
    {
        randomIndex = (rand() % 62);
        password[i] = bank[randomIndex];
    }
    
    //Prints Password
    cout << "Your new password is: " << endl;
    for(int i = 0; i < passwdLength; i++)
    {
        cout << password[i];
    }
    cout << endl; //new line
    delete[] password; //destroy dynamically allocated memory

    return 0;
}
